# Sciomp Packages

A public package registry for scicomp projects, intended to facilitate easy pip / npm installation of our own packages.

## Python Packages
#### [outline-repo-databackend](https://git.uwaterloo.ca/science-computing/outline-quest-databackend)
Interface between outline.uwaterloo.ca and QUEST


## Node Packages
